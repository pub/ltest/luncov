/* run.config
   STDOPT: +"-luncov-eva"
*/

/* Generated by Frama-C LTest */

#ifndef pc_label
#define pc_label(...) do{}while(0)
#endif
#ifndef pc_label_bindings
#define pc_label_bindings(...) do{}while(0)
#endif

int main(int a)
 {
   int __retres;
   int __SEQ_STATUS_a_1 = 1;
   int __SEQ_STATUS_a_2 = 0;
   int __SEQ_STATUS_a_3 = 0;
   int __SEQ_STATUS_a_4 = 0;
   int __SEQ_STATUS_a_5 = 0;
   int __SEQ_STATUS_a_6 = 0;
   int __SEQ_STATUS_a_7 = 0;
   int __SEQ_STATUS_a_8 = 1;
   int __SEQ_STATUS_a_9 = 0;
   int __SEQ_STATUS_a_10 = 0;
   int __SEQ_STATUS_a_11 = 0;
   int __SEQ_STATUS_a_12 = 0;
   int __SEQ_STATUS_a_13 = 0;
   int __SEQ_STATUS_a_14 = 0;
   __SEQ_STATUS_a_1 = 0;
   __SEQ_STATUS_a_8 = 0;
   a = 0;
   __SEQ_STATUS_a_2 = 1;
   __SEQ_STATUS_a_9 = 1;
   __SEQ_STATUS_a_1 = 0;
   __SEQ_STATUS_a_2 = 0;
   __SEQ_STATUS_a_8 = 0;
   __SEQ_STATUS_a_9 = 0;
   a = 1;
   __SEQ_STATUS_a_3 = 1;
   __SEQ_STATUS_a_10 = 1;
   __SEQ_STATUS_a_1 = 0;
   __SEQ_STATUS_a_2 = 0;
   __SEQ_STATUS_a_3 = 0;
   __SEQ_STATUS_a_8 = 0;
   __SEQ_STATUS_a_9 = 0;
   __SEQ_STATUS_a_10 = 0;
   a = 2;
   __SEQ_STATUS_a_4 = 1;
   __SEQ_STATUS_a_11 = 1;
   __SEQ_STATUS_a_1 = 0;
   __SEQ_STATUS_a_2 = 0;
   __SEQ_STATUS_a_3 = 0;
   __SEQ_STATUS_a_4 = 0;
   __SEQ_STATUS_a_8 = 0;
   __SEQ_STATUS_a_9 = 0;
   __SEQ_STATUS_a_10 = 0;
   __SEQ_STATUS_a_11 = 0;
   a = 3;
   __SEQ_STATUS_a_5 = 1;
   __SEQ_STATUS_a_12 = 1;
   __SEQ_STATUS_a_1 = 0;
   __SEQ_STATUS_a_2 = 0;
   __SEQ_STATUS_a_3 = 0;
   __SEQ_STATUS_a_4 = 0;
   __SEQ_STATUS_a_5 = 0;
   __SEQ_STATUS_a_8 = 0;
   __SEQ_STATUS_a_9 = 0;
   __SEQ_STATUS_a_10 = 0;
   __SEQ_STATUS_a_11 = 0;
   __SEQ_STATUS_a_12 = 0;
   a = 4;
   __SEQ_STATUS_a_6 = 1;
   __SEQ_STATUS_a_13 = 1;
   __SEQ_STATUS_a_1 = 0;
   __SEQ_STATUS_a_2 = 0;
   __SEQ_STATUS_a_3 = 0;
   __SEQ_STATUS_a_4 = 0;
   __SEQ_STATUS_a_5 = 0;
   __SEQ_STATUS_a_6 = 0;
   __SEQ_STATUS_a_8 = 0;
   __SEQ_STATUS_a_9 = 0;
   __SEQ_STATUS_a_10 = 0;
   __SEQ_STATUS_a_11 = 0;
   __SEQ_STATUS_a_12 = 0;
   __SEQ_STATUS_a_13 = 0;
   a = 5;
   __SEQ_STATUS_a_7 = 1;
   __SEQ_STATUS_a_14 = 1;
   pc_label(__SEQ_STATUS_a_1 == 1,1,"ADC");
   pc_label(__SEQ_STATUS_a_2 == 1,2,"ADC");
   pc_label(__SEQ_STATUS_a_3 == 1,3,"ADC");
   pc_label(__SEQ_STATUS_a_4 == 1,4,"ADC");
   pc_label(__SEQ_STATUS_a_5 == 1,5,"ADC");
   pc_label(__SEQ_STATUS_a_6 == 1,6,"ADC");
   pc_label(__SEQ_STATUS_a_7 == 1,7,"ADC");
   pc_label(__SEQ_STATUS_a_8 == 1,8,"ADC");
   pc_label(__SEQ_STATUS_a_9 == 1,9,"ADC");
   pc_label(__SEQ_STATUS_a_10 == 1,10,"ADC");
   pc_label(__SEQ_STATUS_a_11 == 1,11,"ADC");
   pc_label(__SEQ_STATUS_a_12 == 1,12,"ADC");
   pc_label(__SEQ_STATUS_a_13 == 1,13,"ADC");
   pc_label(__SEQ_STATUS_a_14 == 1,14,"ADC");
   __retres = a + a;
   return __retres;
 }
